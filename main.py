#!/usr/bin/env python3

import string
import abc
import random
import csv
import unicodedata
import argparse
import os
import sys
import subprocess
import sqlite3

defaultDir = os.path.dirname(sys.argv[0])
debug = False

class DictCommentReader(csv.DictReader) :
    def __init__(self, f, commentsymbol = None, **kwargs) :
        if commentsymbol is None :
            super().__init__(filter(lambda row: row.strip() != "", f), **kwargs)
        else :
            super().__init__(filter(lambda row: row.strip() != "" and not row.startswith(commentsymbol), f), **kwargs)
            
class MyDialect(csv.Dialect) :
    delimiter = ";"
    quotechar = "µ"
    skipinitialwhitespace = True
    quoting = csv.QUOTE_MINIMAL
    lineterminator = "\r\n"
    

class sqlite3dict(sqlite3.Connection) :
    @staticmethod
    def dict_factory(cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d
    
    def __init__(self, fn) :
        super().__init__(fn)
        self.row_factory = sqlite3dict.dict_factory

def xdg_open(fn) :
    try:
        subprocess.Popen(["xdg-open", fn], stdin=None, stdout=None, stderr=None)
    except:
        print("L'ouverture du fichier a échoué, ouvrez-le manuellement depuis", fn)

def sans_accent(s) :
    """Normalise une chaine"""
    return unicodedata.normalize("NFKD", s).encode("ASCII", "ignore").decode("utf-8")

class LaTeXTemplate(string.Template) :
    delimiter = "µ"
    
class BeamerGen :
    def __init__(self, args) :
        self.main_template = LaTeXTemplate(open(args.maintemp_fn).read())
        self.frame_template = LaTeXTemplate(open(args.frametemp_fn).read())
        self.out_fn = args.out_fn
        self.content = ""
        self.globdict = getattr(args, "globdict", {})
        self.argsdict = {"classes" : ", ".join(getattr(args, "classes", [])), "option" : getattr(args, "option", ""), "chapitres" : ", ".join(str(x) for x in getattr(args, "chapitres", set()))}
    
    def addFrame(self, framedict) :
        self.content += self.frame_template.substitute({**self.argsdict, **self.globdict, **framedict})
    
    def finalize(self) :
        base, ext = os.path.splitext(self.out_fn)
        if ext == ".pdf" :
            fn_tex = base+".tex"
        else :
            fn_tex = self.out_fn
        with open(fn_tex, "w") as out :
            out.write(self.main_template.substitute({**self.argsdict, **self.globdict, "content": self.content}))
        if ext == ".pdf" :
            subprocess.call(["latexmk", "-lualatex", "-bibtex-cond", "-cd", "-interaction=batchmode",  "-halt-on-error",  fn_tex])
            if not debug :
                subprocess.call(["latexmk", "-cd", "-c", "-e", "$clean_ext .= q/ xdv/;", fn_tex])
                os.remove(fn_tex)
            xdg_open(base + ".pdf")

class Source :
    @abc.abstractclassmethod
    def register(cls, group) :
        raise NotImplementedError

class QuestionsSource(Source) :
    def __init__(self, args) :
        if hasattr(args, "test_mode") and args.test_mode :
            self.test_mode = True
        else :
            self.test_mode = False
    
    @abc.abstractmethod
    def __len__(self) :
        raise NotImplementedError
        
    @abc.abstractmethod    
    def __getitem__(self, i) :
        raise NotImplementedError
        
    def __iter__(self) :
        if self.test_mode :
            return (self.__getitem__(i) for i in range(self.__len__()))
        else :
            return (self.__getitem__(i) for i in random.sample(range(self.__len__()), self.__len__()))
        
class QuestionsSourceCSV(QuestionsSource) :
    def __init__(self, args) :
        super().__init__(args)
        f = open(args.questions_fn, newline="")
        critere = lambda x: args.chapitres is None or int(x["chapitre"]) in args.chapitres
        self.contenu = [x for x in DictCommentReader(f, dialect=MyDialect, commentsymbol="%") if critere(x)]
        f.close()
    
    def __len__(self) :
        return len(self.contenu)
    
    def __getitem__(self, i) :
        return self.contenu[i]
        
    @classmethod
    def register(cls, group) :
        group.add_argument("--questcsv", help="Questions depuis fichier CSV.", action=cls.Action, nargs="?", default=os.path.join(defaultDir, "questions.csv"))
    
    class Action(argparse.Action) :
        def __call__(self, parser, args, values, option_string=None) :
            if values is None :
                values = self.default
            args.sQ = QuestionsSourceCSV
            args.questions_fn = values
            
            
class QuestionsSourceCSVDir(QuestionsSource) :
    def __init__(self, args) :
        super().__init__(args)
        self.contenu = []
        for root, _, files in os.walk(args.questions_fn) :
            for fn in files :
                base, ext = os.path.splitext(fn)
                if ext != ".csv" or base[0] in ("#", "~", ".") or base[-1] in ("#", "~") :
                    continue
                chapitre = None
                for x in base.split("-") :
                    if x.isdigit() :
                        chapitre = int(x)
                        break
                if args.chapitres is not None and chapitre not in args.chapitres :
                    continue
                csvfn = os.path.join(root, fn)        
                f = open(csvfn, newline="")
                for x in DictCommentReader(f, dialect=MyDialect, commentsymbol="%") :
                    x["fnroot"] = root
                    x["fnbase"] = base
                    x["chapitre"] = str(chapitre)
                    self.contenu.append(x)
                f.close()

    def __len__(self) :
        return len(self.contenu)
    
    def __getitem__(self, i) :
        return self.contenu[i]
        
    @classmethod
    def register(cls, group) :
        group.add_argument("--questcsvdir", help="Questions depuis un dossier de fichiers CSV.", action=cls.Action, nargs="?", default=os.path.join(defaultDir, "questions"))
    
    class Action(argparse.Action) :
        def __call__(self, parser, args, values, option_string=None) :
            if values is None :
                values = self.default
            args.sQ = QuestionsSourceCSVDir
            args.questions_fn = values
        
class ElevesSource(Source) :
    def __init__(self, args) :
        pass
        
    @abc.abstractmethod
    def __len__(self) :
        raise NotImplementedError
        
    @abc.abstractmethod
    def __getitem__(self, i) :
        raise NotImplementedError
        
    @abc.abstractmethod    
    def __iter__(self) :
        raise NotImplementedError

class Test(ElevesSource) :
    def __len__(self) :
        return 0

    @classmethod
    def register(cls, group) :
        group.add_argument("--test", nargs=0, help="Compiler toutes les questions.", action=cls.Action)
    
    class Action(argparse.Action) :
        def __call__(self, parser, args, values, option_string=None) :
            args.sE = Test
            args.test_mode = True

class ElevesSourceCSV(ElevesSource) :
    def __init__(self, args) :
        f = open(args.eleves_fn, newline="")
        critere = lambda x : (args.classe is None or x["classe"] in args.classes) and (args.option is None or x["option"] == args.option)
        self.contenu = [x for x in DictCommentReader(f, dialect=MyDialect, commentsymbol="%") if critere(x)]
        f.close()
        self.contenu.sort(key= lambda x: (sans_accent(x["nom"]), sans_accent(x["prenom"])))
    
    def __len__(self) :
        return len(self.contenu)
    
    def __getitem__(self, i) :
        return self.contenu[i]
        
    def __iter__(self) :
        return iter(self.contenu)
        
    @classmethod
    def register(cls, group) :
        group.add_argument("--elevescsv", help="Élèves depuis fichier CSV", nargs="?", action=cls.Action, default=os.path.join(defaultDir, "eleves.csv"))
    
    class Action(argparse.Action) :
        def __call__(self, parser, args, values, option_string=None) :
            if values is None :
                values = self.default
            args.sE = ElevesSourceCSV
            args.eleves_fn = values

            
class ElevesSourceSQLite(ElevesSource) :
    
    @staticmethod
    def ordonner_nom(s1, s2) :
        """Relation d'ordre pour la BDD"""
        t1 = sans_accent(s1)
        t2 = sans_accent(s2)
        if t1 < t2 :
            return -1
        elif t1==t2 :
            return 0
        else :
            return 1
    
    def __init__(self, args) :
        super().__init__(args)
        bdd = sqlite3dict(args.eleves_fn)
        bdd.create_collation("ORDONNER_NOMS", ElevesSourceSQLite.ordonner_nom)

        wherestr = " WHERE 1=1"
        wheretupl = ()
        
        if args.classes is not None :
            wherestr += " AND classe in (" + ",".join(["?" for _ in range(len(args.classes))]) + ")"
            wheretupl += tuple(args.classes)
        if args.option is not None :
            wherestr += " AND option = ?"
            wheretupl += (args.option,)
        
        self.contenu = bdd.execute("SELECT * FROM " + args.eleves_table + wherestr + " ORDER BY nom COLLATE ORDONNER_NOMS, prenom COLLATE ORDONNER_NOMS", wheretupl).fetchall()
        bdd.close()
    
    def __len__(self) :
        return len(self.contenu)
    
    def __getitem__(self, i) :
        return self.contenu[i]
        
    def __iter__(self) :
        return iter(self.contenu)
        
    @classmethod
    def register(cls, group) :
        group.add_argument("--elevessqlite", help="Élèves depuis fichier SQLite. Préciser la table après une virgule si différente de etudiants.", action=cls.Action, nargs="?", default=os.path.join(defaultDir, "eleves.sqlite")+",etudiants")
    
    class Action(argparse.Action) :
        def __call__(self, parser, args, values, option_string=None) :
            if values is None :
                values = self.default
            fnt = values.split(",")
            if len(fnt) == 1 :
                fnt.append("etudiants")
            args.sE = ElevesSourceSQLite
            args.eleves_fn = fnt[0]
            args.eleves_table = fnt[1]
        
class Generation :
    @staticmethod
    def generer(sQ, sE, args) :
        if len(sE) > len(sQ) :
            raise ValueError("Il n'y a que " + str(len(sQ)) + " questions mais il y a " + str(len(sE)) + " élèves.")
        out = BeamerGen(args)
        if hasattr(args, "test_mode") and args.test_mode :
            print("Test des", len(sQ), "questions.")
            eleve = {"nom" : "nom", "prenom" : "prenom", "classe" : "cl", "option": "opt"}
            for question in sQ :
                out.addFrame({**eleve, **question})
        else :
            print("Sélection de questions pour", len(sE), "élèves parmi", len(sQ), "questions.")
            sQ = iter(sQ)
            sE = iter(sE)
            for eleve in sE :
                question = next(sQ)
                out.addFrame({**eleve, **question})
        out.finalize()

class ActionIntervalles(argparse.Action) :
    def __call__(self, parser, args, values, option_string=None) :
        chapitres = set()
        a_enlever = set()
        for bloc in values :
            if bloc[0] == "~" or bloc[0] == "^" :
                ens = a_enlever
                bloc = bloc[1:]
            else :
                ens = chapitres
            bornes = bloc.split('-')
            if len(bornes) == 1 :
                ens.update({int(bornes[0])})
            elif len(bornes) == 2 :
                ens.update({x for x in range(int(bornes[0]), int(bornes[1])+1)})
            else :
                raise ValueError("Intervalle mal formé : " + bloc)
        chapitres.difference_update(a_enlever)
        setattr(args, self.dest, chapitres)

class ActionDict(argparse.Action) :
    def __call__(self, parser, args, values, option_string=None) :
        cle, valeur = values
        dict = getattr(args, self.dest, {})
        dict[cle] = valeur


def main() :
    global debug

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    
    parser.add_argument("--main-template", help="Fichier du modèle de document .tex principal.", dest="maintemp_fn", default=os.path.join(defaultDir, "gabarit_doc.tex"))
    parser.add_argument("--frame-template", help="Fichier du modèle de document .tex pour chaque page.", dest="frametemp_fn", default=os.path.join(defaultDir, "gabarit_frame.tex"))
    parser.add_argument("out_fn", help="Nom du fichier de sortie. Extension .tex ou .pdf pour une compilation dans la foulée (nécessite latexmk et lualatex).")
    parser.add_argument("--debug", help="Mode débogage (n'efface pas les fichiers de compilation tex).", action="store_true", default=False)
    parser.add_argument("--option", help = "Ne retenir que les élèves de telle option.")
    parser.add_argument("--classes", nargs="+", help= "Ne retenir que les élèves de telles classes (liste séparée par espaces).")
    parser.add_argument("--chapitres", nargs="+", help="Ne retenir que les questions de tels chapitres (format 1 3-7 10).", action=ActionIntervalles, metavar="(CHAP | CHAP-CHAP | ~CHAP | ~CHAP-CHAP)")
    parser.add_argument("--d", nargs=2, dest="globdict", help="Élément supplémentaire du dictionnaire (clé valeur). Utilisable plusieurs fois", default={}, action=ActionDict, metavar=("CLE", "VALEUR"))
    
    sources_eleves = parser.add_mutually_exclusive_group(required=True)
    for x in ElevesSource.__subclasses__():
        x.register(sources_eleves)
    
    sources_questions = parser.add_mutually_exclusive_group(required=True)
    for x in QuestionsSource.__subclasses__() :
        x.register(sources_questions)
    
    args = parser.parse_args()
    if not hasattr(args, "debug") : args.debug = False
    debug = args.debug
    Generation.generer(args.sQ(args), args.sE(args), args)
    
    
if __name__ == "__main__" :
    pass
    main()