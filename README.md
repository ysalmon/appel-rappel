## Appel-Rappel
Pourquoi faire l'appel en demandant à chaque élève de répondre « Présent » quand on peut en profiter pour poser à chacun d'eux une question à réponse courte sur le cours de la séance précédente ?

Le présent script automatise la génération d'une présentation (beamer) à partir d'une liste d'élèves et d'une liste de questions et réponses : on peut donc vidéoprojeter successivement la question posée à chaque élève, puis afficher la réponse avant de passer à l'élève suivant. Les questions sont prises dans un ordre aléatoire parmi la liste fournie.

### Prérequis
  * Python3 et ses modules standard
  * Si vous utilisez la compilation automatique, une distribution LaTeX avec latexmk et luaLaTeX (sinon on peut toujours se contenter de générer un fichier .tex et le traiter ensuite à la main)

### Utilisation
On a besoin de trois choses :
  *  Des gabarits LaTeX pour le document en tant que tel (cela permet d'ajuster le préambule, notamment) et pour le contenu de chaque ```frame``` beamer. Voir les fichiers .tex fournis en exemple.
  *  Une liste de questions et réponses sous forme de code LaTeX qui sera inséré dans le gabarit. Actuellement cette liste doit être dans un format CSV dont la première ligne donne le nom des colonnes : ```question```, ```reponse``` et ```chapitre``` (lequel doit être un entier). Alternativement, on peut indiquer un dossier qui contient des fichiers CSV par chapitre (en ce cas, il n'y a pas lieu d'avoir une colonne ```chapitre``` dans les fichiers : c'est le nom des fichiers CSV qui les rattache à un chapitre).
  * Une liste d'élèves. Actuellement, elle peut se trouver dans un fichier CSV dont la première ligne donne le nom des colonnes :  ```nom```, ```prenom```, ```classe```, ```option``` ces deux derniers éléments permettant de générer un fichier pour les seuls élèves d'une ou plusieurs classes, éventuellement suivant telle option. Alternativement, ces données peuvent se trouver dans une table d'une base SQLite (avec les mêmes noms d'attributs que ci-dessus).

Pour appeler le script, il faut au minimum donner le nom du fichier de sortie. Si le nom porte l'extension ```.pdf```, un fichier ```.tex``` de même nom sera généré puis compilé automatiquement et enfin affiché.

Il faut aussi indiquer les sources de données :
   * la liste des questions avec l'option ```--questcsv``` (le cas échéant suivi du nom de fichier ; par défaut questions.csv dans le répertoire courant) ou bien ```--questcsvdir``` si les questions sont dans plusieurs fichiers (en ce cas, le nom des fichiers doit contenir le numéro de chapitre entre caractères ```-```).
   * la liste des élèves avec l'une (et une seule) des deux options ```--elevescsv``` ou ```--elevessqlite``` (même remarque).

On peut enfin spécifier des noms de fichiers particuliers pour les gabarits ou restreindre les élèves concernés comme indiqué ci-dessus. On peut aussi ne prendre que des questions de certains chapitres à l'aide d'une spécification de la forme ```3 7-12 15``` pour les chapitres 3, 7 à 12 inclus, et 15. La spécification des chapitres peut exclure certains chapitres : ```1-20 ~5-7``` correspond aux chapitres 1 à 20 inclus mais sans les chapitres 5, 6 et 7.

Attention, le nombre de questions doit être supérieur au nombre d'élèves ; il n'y a aucune répétition des questions.

#### Notes

Si les sources de données contiennent de champs supplémentaires, ceux-ci peuvent être utilisés dans les gabarits. Il convient que les ensembles de champs des élèves et des questions soient disjoints.

Dans les fichiers CSV, les lignes commençant par ```%``` sont ignorées. En dehors du début de ligne, ce symbole ne reçoit pas de traitement particulier et se retrouvera tel quel dans le code tex généré. Les colonnes sont séparées par des ```;``` (les espaces situées après chaque séparateur sont ignorées). Si une colonne contient le caractère de séparation, cette colonne doit être encadrée par le caractère ```µ``` (c'est plus pratique que des guillemets parce que c'est d'emploi bien plus rare).

On peut ajouter des éléments utilisables par les modèles avec l'option ```--d``` suivie d'une clé et de la valeur associée. Cette option est utilisable plusieurs fois. C'est utile par exemple pour donner un titre apparaissant dans le fichier généré. À nouveau, les clés doivent être différentes de celles utilisées dans les fichiers de sources.

### Extensions possibles
#### Nouveaux formats de sources
On peut ajouter la possibilité d'utiliser des sources de données différentes (par exemple une base SQLite pour les questions) en définissant un nouvelle classe fille de ```QuestionsSource``` ou ```ElevesSource```. On prendra modèle sur les classes existantes, qui font deux choses :
   * leur boulot de source de données avec les méthodes ```__init__```, ```__len__```, ```__getitem__``` et ```__iter__``` ;
   * leur enregistrement dans l'interface principale (création d'une option sur la ligne de commande pour sélectionner cette source) au moyen de la méthode de classe ```register``` et de la classe imbriquée ```Action``` (on veillera en particulier à y définir correctement ```args.sE``` ou ```args.sQ```).

L'enregistrement dans l'interface principale est automatique parce que ```main``` énumère toutes les classes filles de ```QuestionsSource``` et ```ElevesSource```.

#### Poids des différents chapitres
En rédigeant ce document, je me rends compte qu'au delà de la spécification des chapitres à retenir, il serait intéressant de pouvoir moduler la proportion de questions des différents chapitres. À l'heure actuelle, il est simplement procédé à un filtrage des questions puis à une permutation aléatoire de la liste, de sorte que les chapitres ayant plus de questions seront statistiquement plus représentés.

#### Schémas de questions
Il serait peut-être intéressant de pouvoir disposer de schémas de questions, c'est-à-dire des questions dans lesquelles certains éléments, comme des données numériques, seraient générées aléatoirement par exemple. Cependant, il est déjà possible d'y pourvoir par un pré-traitement du fichier des questions.
